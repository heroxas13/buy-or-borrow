#図書検索サービス　buy or borrow

これは図書検索して、在住の市町村の図書館の貸し出し貸し出し情報と予約リンク、オンラインショップでの価格と購買リンク
が表示されるアプリケーション

## 使い方

このアプリケーションを動かす場合は、まずはリポジトリを手元にクローンしてください。
その後、次のコマンドで必要になる RubyGems をインストールします。

```
$ bundle install --without production
```

その後、データベースへのマイグレーションを実行します。

```
$ rails db:migrate
```

最後に、テストを実行してうまく動いているかどうか確認してください。

```
$ rails test
```

テストが無事に通ったら、Railsサーバーを立ち上げる準備が整っているはずです。

```
$ rails server
```



$ git commit -am "Improve the README"
